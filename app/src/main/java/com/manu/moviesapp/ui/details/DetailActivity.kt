package com.manu.moviesapp.ui.details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.navArgs
import com.bumptech.glide.Glide
import com.manu.moviesapp.databinding.ActivityDetailBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class DetailActivity : AppCompatActivity() {


    private lateinit var binding: ActivityDetailBinding
    private val detailViewModel: DetailViewModel by viewModels()
    private val url_path = "https://image.tmdb.org/t/p/original"

    private val args: DetailActivityArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        detailViewModel.getMovieDetail(args.movieId)
        initUI()
    }

    private fun initUI() {
        initListeners()
        initUIState()
    }

    private fun initListeners() {
        binding.ivBackDetail.setOnClickListener { onBackPressedDispatcher.onBackPressed() }
    }

    private fun initUIState() {
        lifecycleScope.launch {
            repeatOnLifecycle(androidx.lifecycle.Lifecycle.State.STARTED) {
                detailViewModel.state.collect {
                    when(it) {
                        is MovieDetailState.Error -> errorState()
                        MovieDetailState.Loading -> loadingState()
                        is MovieDetailState.Success -> successState(it)
                    }
                }
            }
        }
    }

    private fun successState(state: MovieDetailState.Success) {
        val movieDetail = state.movieDetail
        binding.detailProgressBar.isVisible = false
        binding.tvTitleDetal.text = movieDetail.title
        Glide.with(binding.ivDetails.context)
            .load(url_path + movieDetail.posterPath)
            .into(binding.ivDetails)
        binding.tvAverageVote.text = "%.1f".format(movieDetail.voteAverage)
        binding.tvRuntime.text = "${movieDetail.runtime.toString()} min"
        val genres: String = movieDetail.genres?.map { it.name }?.joinToString(", ") ?: "Sin genero"
        binding.tvGenres.text = genres
        binding.tvOverview.text = movieDetail.overview
    }

    private fun errorState() {
        binding.detailProgressBar.isVisible = false
    }

    private fun loadingState() {
        binding.detailProgressBar.isVisible = true
    }
}