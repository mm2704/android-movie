package com.manu.moviesapp.ui.movies

import com.manu.moviesapp.domain.model.Movie
import com.manu.moviesapp.ui.details.MovieDetailState

sealed class MoviesState {

    data class Error(val error: String) : MoviesState()
    data class Success(val movies: List<Movie>) : MoviesState()
}