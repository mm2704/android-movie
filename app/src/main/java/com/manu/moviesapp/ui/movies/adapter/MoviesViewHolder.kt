package com.manu.moviesapp.ui.movies.adapter

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.manu.moviesapp.databinding.ItemMovieBinding
import com.manu.moviesapp.domain.model.Movie

class MoviesViewHolder(private val binding: ItemMovieBinding) :
    RecyclerView.ViewHolder(binding.root) {

    private val url_path = "https://image.tmdb.org/t/p/original"

    fun render(movie: Movie, onMovieSelected: (Movie) -> Unit) {
        Glide.with(binding.ivMovie.context)
            .load(url_path + movie.posterPath)
            .apply(RequestOptions.encodeQualityOf(1))
            .into(binding.ivMovie)

        binding.itemView.setOnClickListener {
            onMovieSelected(movie)
        }
    }
}