package com.manu.moviesapp.ui.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.manu.moviesapp.domain.usecase.GetMovieDetail
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(private val getMovieDetail: GetMovieDetail) :
    ViewModel() {

    private var _state = MutableStateFlow<MovieDetailState>(MovieDetailState.Loading)
    val state: StateFlow<MovieDetailState> = _state

    fun getMovieDetail(id: Long) {
        viewModelScope.launch {
            _state.value = MovieDetailState.Loading
            val result = withContext(Dispatchers.IO) { getMovieDetail.getMovieDetail(id) }
            if(result != null) {
                _state.value = MovieDetailState.Success(result)
            } else {
                _state.value = MovieDetailState.Error("Ocurrio un error, intentelo mas tarde")
            }
        }
    }
}