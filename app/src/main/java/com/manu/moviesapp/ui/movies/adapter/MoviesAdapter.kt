package com.manu.moviesapp.ui.movies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.manu.moviesapp.databinding.ItemMovieBinding
import com.manu.moviesapp.domain.model.Movie

class MoviesAdapter(private var movieList: List<Movie> = emptyList(), private val onMovieSelected: (Movie) -> Unit) : RecyclerView.Adapter<MoviesViewHolder>() {

    fun updateMovies(newMovies: List<Movie>) {
        movieList = newMovies
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val binding = ItemMovieBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return MoviesViewHolder(binding)
    }

    override fun getItemCount() = movieList.size

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        holder.render(movieList[position], onMovieSelected)
    }
}