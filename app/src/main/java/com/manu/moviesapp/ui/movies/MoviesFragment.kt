package com.manu.moviesapp.ui.movies

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.manu.moviesapp.databinding.FragmentMoviesBinding
import com.manu.moviesapp.domain.model.Movie
import com.manu.moviesapp.ui.movies.adapter.MoviesAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MoviesFragment : Fragment() {

    private val moviesViewModel by viewModels<MoviesViewModel>()

    private lateinit var movieAdapter: MoviesAdapter

    private var _binding: FragmentMoviesBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMoviesBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
        moviesViewModel.getMovies(1)
        Log.i("logTest", "Obtengo movies del viewCreated")
    }

    private fun initUI() {
        initRecyclerView()
        initUIState()
    }

    private fun initRecyclerView() {
        movieAdapter = MoviesAdapter { movie ->
            onMovieClicked(movie)
        }

        val gridLayout = GridLayoutManager(context, 2)

        binding.rvMovies.apply {
            adapter = movieAdapter
            layoutManager = gridLayout

            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    val lastVisibleItemPosition = gridLayout.findLastVisibleItemPosition()
                    if (lastVisibleItemPosition == gridLayout.itemCount - 1) {
                        //if(gridLayout.findViewByPosition(lastVisibleItemPosition)?.right == recyclerView.right) {
                            moviesViewModel.getMovies(null)
                       // }
                    }
                }
            })
        }
    }

    private fun initUIState() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                moviesViewModel.state.collect {
                    when (it) {
                        is MoviesState.Error -> errorState()
                        is MoviesState.Success -> successState(it)
                    }
                }
            }
        }
    }

    private fun errorState() {
        movieAdapter.updateMovies(listOf())
    }

    private fun successState(it: MoviesState.Success) {
        Log.i("logTest", "seteo pelicula en movielist del fragment, peliculas: ${it.movies.size}")
        movieAdapter.updateMovies(it.movies)
    }

    private fun onMovieClicked(movie: Movie) {
        findNavController().navigate(
            MoviesFragmentDirections.actionMoviesFragmentToDetailActivity(
                movie.id!!
            )
        )
    }

}