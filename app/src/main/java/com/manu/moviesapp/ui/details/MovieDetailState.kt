package com.manu.moviesapp.ui.details

import com.manu.moviesapp.domain.model.MovieDetail

sealed class MovieDetailState {
    data object Loading : MovieDetailState()
    data class Error(val error: String) : MovieDetailState()
    data class Success(val movieDetail: MovieDetail) : MovieDetailState()
}