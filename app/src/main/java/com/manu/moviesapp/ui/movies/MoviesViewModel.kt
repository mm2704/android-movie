package com.manu.moviesapp.ui.movies

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.manu.moviesapp.domain.model.Movie
import com.manu.moviesapp.domain.usecase.GetMovies
import com.manu.moviesapp.ui.details.MovieDetailState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel @Inject constructor(private val getMovies: GetMovies) : ViewModel() {

    private var _state = MutableStateFlow<MoviesState>(MoviesState.Success(emptyList()))
    val state: StateFlow<MoviesState> = _state
    private var currentPage = 1
    private var movieList = mutableListOf<Movie>()

    fun getMovies(page: Int?) {
        val resultPageSort = page ?: currentPage
        val resultMovies = arrayListOf<Movie>()
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) { getMovies.getMovies(resultPageSort) }
            if (result != null) {
                if(resultPageSort == 1) {
                    Log.i("logTest", "borrando lista")
                    movieList.clear()
                }
                movieList.addAll(result)
                resultMovies.addAll(movieList)
                _state.value = MoviesState.Success(resultMovies)
                currentPage = resultPageSort+1
            } else {
                _state.value = MoviesState.Error("Ocurrio un error, intentelo mas tarde")
            }
        }
    }
}