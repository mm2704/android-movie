package com.manu.moviesapp.domain

import com.manu.moviesapp.domain.model.Movie
import com.manu.moviesapp.domain.model.MovieDetail

interface Repository {

    suspend fun getMovieDetail(id: Long): MovieDetail?

    suspend fun getMovies(page: Int) : List<Movie>?
}