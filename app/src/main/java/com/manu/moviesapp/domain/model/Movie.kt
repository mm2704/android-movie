package com.manu.moviesapp.domain.model


data class Movie (
    val id: Long?,
    val title: String?,
    val posterPath: String?
)

/*
sealed class Movie(val id: Long, val title: String, val posterPath: String) {
    object Peli1 : Movie(787699, "Wonka", "/tFyQa5WQqldIL44HBLaCmn5eERD.jpg")
    object Peli2 : Movie(572802, "Aquaman Y El Reino Perdido", "/3OdMJBOGkp62e0DfY4ZykL9RuCP.jpg")
    object Peli3 : Movie(866398, "The Beekeeper", "/bHZQYKJV1rV01S3kNZ3hEYz8VNM.jpg")
    object Peli4 : Movie(940551, "Migration", "/brAqSBoSXzxEYV6rGYwbRvhlXCE.jpg")
    object Peli5 :
        Movie(933131, "Cazadores en tierra inhóspita", "/jPGbcbMlzGUECukPhkEMVt1Kvs5.jpg")

    object Peli6 : Movie(1212073, "60 minutos", "/cND79ZWPFINDtkA8uwmQo1gnPPE.jpg")
    object Peli7 : Movie(982940, "Palido", "/weurvfmsfMIIQUegn6J4TJPFlRo.jpg")
    object Peli8 : Movie(799155, "Misión Ataque", "/mJUZAEK6haKyxohs58ZOLqiUb9P.jpg")
    object Peli9 : Movie(753342, "Napoleon", "/re6SSQS1HgBGG9AMiNuPGISOfMx.jpg")
    object Peli10 : Movie(965571, "Время патриотов", "/yrafAmPQgr5RWEe67BrbHu7jiB.jpg")
}*/