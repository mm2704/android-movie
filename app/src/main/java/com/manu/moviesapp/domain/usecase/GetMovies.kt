package com.manu.moviesapp.domain.usecase

import com.manu.moviesapp.domain.Repository
import com.manu.moviesapp.domain.model.Movie
import javax.inject.Inject

class GetMovies @Inject constructor(private val repository: Repository) {

    suspend fun getMovies(page: Int) : List<Movie>? {
        return repository.getMovies(page)
    }
}