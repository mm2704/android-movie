package com.manu.moviesapp.domain.usecase

import com.manu.moviesapp.domain.Repository
import com.manu.moviesapp.domain.model.MovieDetail
import javax.inject.Inject

class GetMovieDetail @Inject constructor(private val repository: Repository) {


    suspend fun getMovieDetail(id: Long): MovieDetail? {
        return repository.getMovieDetail(id)
    }
}