package com.manu.moviesapp.domain.model

data class MovieDetail (

    val title: String?,
    val originalTitle: String?,
    val posterPath: String?,
    val genres: MutableList<Genre>?,
    val overview: String?,
    val releaseDate: String?,
    val runtime: Int?,
    val voteAverage: Float?
    )

data class Genre (
    val id: Int?,
    val name : String?
)
