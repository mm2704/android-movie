package com.manu.moviesapp.data.network

import com.manu.moviesapp.data.network.response.MovieDetailResponse
import com.manu.moviesapp.data.network.response.MoviesResponse
import com.manu.moviesapp.domain.model.Movie
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesApiService {

    @GET("movie/{id}") //otro /discover/movie
    suspend fun getMovieDetail(@Path("id") id: Long): MovieDetailResponse

    @GET("discover/movie")
    suspend fun getMovies(@Query("page") page: Int): MoviesResponse
}