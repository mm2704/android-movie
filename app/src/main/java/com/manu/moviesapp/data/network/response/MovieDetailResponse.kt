package com.manu.moviesapp.data.network.response

import com.google.gson.annotations.SerializedName
import com.manu.moviesapp.domain.model.Genre
import com.manu.moviesapp.domain.model.MovieDetail

data class MovieDetailResponse (

    @SerializedName("title")
    val title: String?,

    @SerializedName("original_title")
    val originalTitle: String?,

    @SerializedName("poster_path")
    val posterPath: String?,

    @SerializedName("genres")
    val genres: MutableList<GenreResponse>?,

    @SerializedName("overview")
    val overview: String?,

    @SerializedName("release_date")
    val releaseDate: String?,

    @SerializedName("runtime")
    val runtime: Int?,

    @SerializedName("vote_average")
    val voteAverage: Float?
    )

{
    fun toDomain() : MovieDetail {
        return MovieDetail(
            title = title,
            originalTitle = originalTitle,
            posterPath = posterPath,
            genres = genres?.map {it.toDomain()}?.toMutableList(),
            overview = overview,
            releaseDate = releaseDate,
            runtime = runtime,
            voteAverage = voteAverage
        )
    }
}

data class GenreResponse (
    @SerializedName("id")
    val id: Int?,
    @SerializedName("name")
    val name : String?
)

{
    fun toDomain() : Genre {
        return Genre(
            id = id,
            name = name
        )
    }
}
