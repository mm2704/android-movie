package com.manu.moviesapp.data.network

import com.manu.moviesapp.BuildConfig
import com.manu.moviesapp.BuildConfig.BASE_URL
import com.manu.moviesapp.data.RepositoryImpl
import com.manu.moviesapp.domain.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(createClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun provideMoviesApiService(retrofit: Retrofit): MoviesApiService {
        return retrofit.create(MoviesApiService::class.java)
    }

    @Provides
    fun provideRepository(apiService: MoviesApiService): Repository {
        return RepositoryImpl(apiService)
    }

    private fun createClient(): OkHttpClient {
        val okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder().apply {
            addInterceptor { chain ->
                val newUrl = chain.request().url()
                    .newBuilder()
                    .addQueryParameter("api_key", "61305534dff128a8f46895a628245736")//"BuildConfig.PRIVATE_KEY"
                    .addQueryParameter("language", "es-ES")
                    .addQueryParameter("include_adult", "false")
                    .addQueryParameter("include_video", "false")
                    //.addQueryParameter("page", "1") //view
                    .addQueryParameter("with_watch_monetization_types", "flatrate")
                    .build()

                val newRequest = chain.request()
                    .newBuilder()
                    .url(newUrl)
                    .build()
                chain.proceed(newRequest)
            }
        }

        return okHttpClientBuilder.build()
    }
}