package com.manu.moviesapp.data.network.response

import com.google.gson.annotations.SerializedName

data class MoviesResponse(

    @SerializedName("results")
    val movies: List<MovieResponse>?

)