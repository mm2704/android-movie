package com.manu.moviesapp.data

import android.util.Log
import com.manu.moviesapp.data.network.MoviesApiService
import com.manu.moviesapp.domain.Repository
import com.manu.moviesapp.domain.model.Movie
import com.manu.moviesapp.domain.model.MovieDetail
import javax.inject.Inject

class RepositoryImpl @Inject constructor(private val apiService: MoviesApiService) : Repository {

    override suspend fun getMovieDetail(id: Long): MovieDetail? {
        runCatching { apiService.getMovieDetail(id) }
            .onSuccess { return it.toDomain() }
            .onFailure { Log.i("retrofitError", "Error: ${it.message}") }

        return null
    }

    override suspend fun getMovies(page: Int): List<Movie>? {
        runCatching { apiService.getMovies(page) }
            .onSuccess {
                return it.movies?.map { movie -> movie.toDomain() }
            }
            .onFailure { Log.i("retrofitError", "Error: ${it.message}") }

        return null
    }
}