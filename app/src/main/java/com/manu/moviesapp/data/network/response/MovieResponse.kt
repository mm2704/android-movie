package com.manu.moviesapp.data.network.response

import com.google.gson.annotations.SerializedName
import com.manu.moviesapp.domain.model.Movie

data class MovieResponse(

    @SerializedName("id")
    val id: Long?,

    @SerializedName("title")
    val title: String?,

    @SerializedName("poster_path")
    val posterPath: String?
) {

    fun toDomain(): Movie {
        return Movie(
            id = id,
            title = title,
            posterPath = posterPath
        )
    }
}
